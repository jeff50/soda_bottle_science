# -*- coding: utf-8 -*-
"""
Created on Tue Sep  4 23:36:43 2018

@author: Jeff2017
"""

# Author: Jeff Davids
# Modified: 
# Company: TU Delft
# Date: 180804
# Date Modified:
# Purpose: Create array of line plots
# v1.0
#
# Instructions: modify file paths, etc. and run
#
#   Import system modules

import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib.patches as mpatches
import seaborn as sns
from datetime import datetime
from pandas import ExcelWriter
from matplotlib import pyplot

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 800)

# coding: utf-8

# define variables

mpl.style.use("seaborn")

inputFilePath = r"C:\_MyDocs\OneDrive\_My_Education\PhD_TUDelft\Docs\Publications\4_CS_Precipitation\Data\Input"
outputFilePath = r"C:\_MyDocs\OneDrive\_My_Education\PhD_TUDelft\Docs\Publications\4_CS_Precipitation\Data\Output"
fileName = "S4W-Nepal_Precip_Data.xlsx"
fileName2 = "S4W-Nepal_CS_Classifications.xlsx"

df = pd.read_excel(inputFilePath + "/" + fileName, sheet_name = "Sheet1", header = 0)
df = df[df["precip_mm"]<=200]
df["datetime"] = pd.to_datetime(df["msmt_datetime"])
df["year-month"] = df["datetime"].dt.strftime("%Y-%m")
df["year-week"] = df["datetime"].dt.strftime("%Y-%W")
df["week"] = df["datetime"].dt.week
df["month"] = df["datetime"].dt.month
df["year"] = df["datetime"].dt.year
df["date"] = df["datetime"].dt.date
df["dayOfWeek"] = df["datetime"].dt.day_name()
df = df[df["MonID"]!="-1_Unknown"]
df = df[df["year"]>=2018]
df = df[df["month"]>=5]
df = df[df["month"]<10]
df.sort_values(by=["datetime"])

df_stats = df.groupby(["MonID"])["datetime"].count()
print("Mean: " + str(round(df_stats.mean(),2)))
print("Min: " + str(round(df_stats.min(),2)))
print("Max: " + str(round(df_stats.max(),2)))
print("Stdev: " + str(round(df_stats.std(),2)))
print("Number of single measurements: " + str(df_stats.where(df_stats == 1).count()))

df_sort = df.groupby(["MonID"])["datetime"].min()
df_sort = df_sort.reset_index()
df_sort = df_sort.sort_values("datetime")

# read in CS classification data
cs = pd.read_excel(inputFilePath + "/" + fileName2, sheet_name = "CS", header = 0)
cs = cs.loc[:,["MonitorID", "RecruitMethod"]]

pivot = pd.pivot_table(df, "precip_mm", "MonID", "year-week", aggfunc = "count")

pivot_sort = pd.merge(df_sort, pivot, how="outer", on="MonID")
pivot_sort["MonID_l"] = pivot_sort["MonID"].str.split("_", expand=True)[1].astype(int)
pivot_sort = pd.merge(pivot_sort, cs, how = "left", 
                      left_on = "MonID_l",
                      right_on = "MonitorID")
pivot_sort = pivot_sort.set_index(["MonID"])

mpl.rcParams.update({'font.size': 20})

labels = ["(a)", "(b)", "(c)", "(d)"]

nRows = 4
nColumns = 1

fig, ax = plt.subplots(nRows, nColumns, dpi=300, figsize=(30, 20))

i = 0

for RecruitMethod in list(pivot_sort["RecruitMethod"].unique()):
    subPlotID = int(str(nRows)+str(nColumns)+str(i+1))
    ax = plt.subplot(subPlotID)
    pivot_plot = pivot_sort[pivot_sort["RecruitMethod"] == RecruitMethod]
    pivot_plot = pivot_plot.drop(["datetime", "MonID_l", "MonitorID", "RecruitMethod"], axis=1)
    
    # use for weekly plot
    with sns.axes_style("white"):
        sns.heatmap(pivot_plot, cmap="Blues", vmin=1, vmax=7, center=3.5, 
                    mask=pivot_plot.isnull(), 
                    cbar= False if i < 3 else True,
                    cbar_kws = dict(use_gridspec=False,location="top"))

    # ADDED: Remove labels.
    frame1 = plt.gca()
    frame1.axes.get_yaxis().set_ticks([])
    ax.set_title("{} {} (n = {})".format(labels[i], RecruitMethod, len(pivot_plot)), size=20)
    ax.set_ylabel("")
    if i < 3:   
        ax.set_xticklabels("", size=20)
    elif i == 3:    
        ax.tick_params(labelsize=16, rotation=30)
        ax.set_xlabel("Year-Week", size=20)
    ax.patch.set_facecolor("white")
    i = i + 1
    print(len(ax.patches))

#fig.text(0.76, 0.5, "Precipitation Measurements Per Week", va='center', 
#         rotation='vertical', size = 20)

# plt.show()

plt.savefig(outputFilePath + '/' + 'CS_Heatmap_Week.png', bbox_inches='tight')

fig.clf()

#########################################
#
## make plot of measurements per day to see if payments have an impact
#
#df_msmtsByDay = df[["date", "precip_mm"]].groupby(["date"]).agg(["mean", "min", "max", "count"]).reset_index()
#df_msmtsByDay.columns = ["Date", "Mean_Precip_mm", "Min_Precip_mm", "Max_Precip_mm", "Number of Measurements"]
#df_msmtsByDay = df_msmtsByDay.set_index(["Date"], drop = False)
#
##df_msmtsPerDay = df.groupby(["dayOfWeek"])["precip_mm"].mean().reset_index()
#df_msmtsPerDay = df.groupby(["dayOfWeek"])["precip_mm"].agg(["mean", "min", "max", "count"]).reset_index()
#df_msmtsPerDay.columns = ["dayOfWeek", "Mean_Precip_mm", "Min_Precip_mm", "Max_Precip_mm", "NumMsmts"]
#df_msmtsPerDay["dayOfWeek"] = pd.Categorical(df_msmtsPerDay["dayOfWeek"], ["Monday",
#              "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"])
#df_msmtsPerDay = df_msmtsPerDay.sort_values(["dayOfWeek"])
#
#sns.scatterplot(df_msmtsPerDay["Mean_Precip_mm"], df_msmtsPerDay["NumMsmts"])
#
#fig, ax = plt.subplots(1, 1, figsize = (6, 5), dpi=300)
#
##sns.lineplot(x = df_msmtsByDay["Date"], y = df_msmtsByDay["Number of Measurements"])
#
#ax = sns.barplot(x = df_msmtsPerDay["dayOfWeek"], y = df_msmtsPerDay["precip_mm"], color="lightsteelblue")
##ax.plot(df_msmtsByDay["Number_Msmts"])
#
#ax.set_ylabel("Number of Measurements Per Day", size=16)    
#ax.set_xlabel("Day of the Week", size=16)
#ax.set_ylim(0, 25)
#ax.tick_params(axis="x", labelsize=14, rotation=45)
#ax.tick_params(axis="y", labelsize=14)
#
#plt.savefig(outputFilePath + '/' + 'Msmts_Per_Day.png', bbox_inches='tight')
#
#fig.clf()
#
## scatter plot of number of measurements as a function of mean precip
#df_msmtsByDay["NextDayNumMsmts"] = 0
#
#for i in range(1, len(df_msmtsByDay)-1):
#    df_msmtsByDay.iloc[i-1,5] = df_msmtsByDay.iloc[i,4]
#
## define dimensions of subplots
#nRows = 1
#nColumns = 2
#
## define fig and ax, resolution, and size in inches
#fig, ax = plt.subplots(nRows, nColumns, dpi=300, figsize=(12, 5.5))
#
#fig.text(0.5, 0.0, "Mean Daily Precipitation (mm)", ha='center', size=16)
#fig.text(0.07, 0.5, "Number of Measurements", va='center', rotation='vertical', size=16)
#
## use i to iterate through
#i = 0
#
## define columns to plot along with titles
#yDataLabels = ["Number of Measurements", "NextDayNumMsmts"]
#subplotTitles = ["(a) Same Day", "(b) Previous Day"]
#
#while i < (nRows * nColumns):
#
#    xData = df_msmtsByDay["Mean_Precip_mm"]
#    yData = df_msmtsByDay[yDataLabels[i]]
#    
#    ax = plt.subplot(nRows, nColumns, i+1)
#    
#    ax = sns.scatterplot(x = xData, y = yData)
#    
#    ax.set_ylabel("")    
#    ax.set_xlabel("")
#    ax.set_title(subplotTitles[i], size = 16)
#    ax.set_xlim(0, 80)
#    ax.set_ylim(0, 80)
#    plt.yticks(ticks = range(0,81,20))
#    ax.tick_params(labelsize=14)
#    
#    r = np.corrcoef(xData, yData)[0,1]
#    r2 = r**2
#    #n = len(xData)
#    n = 154
#    
#    A = np.vstack([xData, np.ones(len(xData))]).T
#    m, b = np.linalg.lstsq(A, yData, rcond = None)[0]
#    
#    plt.plot(xData, m * xData + b, label = "Linear Regression", 
#            linestyle = ":", color = "gray")
#    
#    ax.legend(loc=2, frameon=True, framealpha=0.8, prop={'size': 16})
#    
#    ax.annotate("n = " + str(n), xy=(0.97, 0.23), xycoords="axes fraction", 
#                horizontalalignment="right", size = 16)
#    ax.annotate("y = " + str(round(m,2)) + "x + " + str(round(b,1)), xy=(0.97, 0.17), 
#                xycoords="axes fraction", horizontalalignment="right", size = 16)
#    ax.annotate("r = " + str(round(r,3)), xy=(0.97, 0.11), 
#                xycoords="axes fraction", horizontalalignment="right", size = 16)
#    ax.annotate("r critical = 0.208 (p = 0.01)", xy=(0.97, 0.05), 
#                xycoords="axes fraction", horizontalalignment="right", size = 16)
#    i = i + 1
#
#plt.savefig(outputFilePath + '/' + 'CS_Motivation_Scatter.png', bbox_inches='tight')
#
#fig.clf()
#
##sns.barplot(x = df_msmtsByDay["Date"], y = df_msmtsByDay["Mean_Precip_mm"])
##ax.plot(df_msmtsByDay["Number_Msmts"])