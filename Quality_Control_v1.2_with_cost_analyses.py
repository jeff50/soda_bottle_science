# -*- coding: utf-8 -*-
"""
Created on Thu Sep  6 22:31:06 2018

@author: Jeff2017

"""

import pandas as pd
import numpy as np
import seaborn as sns; sns.set()
import scipy
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from datetime import datetime
from dateutil.parser import parse
from datetime import timedelta
import os

inputFilePath = r"C:\_MyDocs\OneDrive\_My_Education\PhD_TUDelft\Docs\Publications\4_CS_Precipitation\Data\Input"
outputFilePath = r"C:\_MyDocs\OneDrive\_My_Education\PhD_TUDelft\Docs\Publications\4_CS_Precipitation\Data\Output"
fileName = "S4W-Nepal_QCEdits_S4WNepalMain.xlsx"
fileName2 = "S4W-Nepal_Precip_Data.xlsx"
fileName3 = "S4W-Nepal_CS_Classifications.xlsx"
startDate = datetime.strptime("2018-05-01", "%Y-%m-%d")
endDate = datetime.strptime("2018-10-01", "%Y-%m-%d")

f1 = "QC_Ratio_10.png"
f2 = "Fig9_QC_Scatter.tiff"
f3 = "QC_CS_Errors.png"
f4 = "Cost_Per_Observation.png"

qc = pd.read_excel(inputFilePath + "/" + fileName, sheet_name = "Sheet1", header = 0)

qc["Date"] = qc["timeStamp"]

qc = qc[["OdkID", "meta-instance-id", "Monitors", "Date", "Editor", "QCFlag.new", 
        "QCFlag.old", "precip_mm.new", "precip_mm.old"]]

# get rid of any commas in precip_mm and covert to float
qc["precip_mm.old"] = qc["precip_mm.old"].replace("2,0", "2.0")
qc["precip_mm.new"] = qc["precip_mm.new"].replace("2,0", "2.0")
qc["precip_mm.old"] = qc["precip_mm.old"].astype(float)
qc["precip_mm.new"] = qc["precip_mm.new"].astype(float)

qc["datetime"] = pd.to_datetime(qc["Date"])
qc["datetime"] = qc["datetime"] + timedelta(hours=5.75)

odk = pd.read_excel(inputFilePath + "/" + fileName2, sheet_name = "Sheet1", header = 0)

odk["datetime"] = pd.to_datetime(odk["msmt_datetime"])
# convert to local time (LT)
odk["datetime"] = odk["datetime"] + timedelta(hours=5.75)
odk["MonID"] = odk["MonID"].str.replace("-1_Unknown", "-1_999")
odk["MonID"] = odk["MonID"].str.split("_").str[1]
odk["MonID"] = odk["MonID"].apply(int)
odk["meta-instance-id"] = odk["*meta-instance-id*"]
#odk.rename({"*meta-instance-id*": "meta-instance-id"}, axis="columns")

#truncate ODK data between startDate and endDate
odk = odk[(odk["datetime"] >= startDate) & (odk["datetime"] <= endDate)]

qc = pd.merge(qc, odk, how="left", on="meta-instance-id")
qc_lim = qc[qc["MonID"] > 0]

qc_filter = qc_lim.groupby(["meta-instance-id"]).agg({"precip_mm.old": "count", 
                     "datetime_x": "max"})

qc_lim2 = qc_lim[qc_lim["datetime_x"].isin(qc_filter["datetime_x"])]
qcFinal = qc_lim2.dropna(subset=["precip_mm.old"])

pd.options.mode.chained_assignment = None  # default='warn'
qcFinal["editRatio"] = qcFinal["precip_mm.new"].divide(qcFinal["precip_mm.old"])
qcFinal["editDiff"] = qcFinal["precip_mm.old"].subtract(qcFinal["precip_mm.new"])

# only keep edits with changes greater than 1 mm

qcFinal = qcFinal[(qcFinal["editDiff"] > 1) | (qcFinal["editDiff"] < -1)]

# calculate error metrics

qcFinal = qcFinal.replace([np.inf, -np.inf], np.nan)

# There are a few errors that came from weird circumstances like the long term
# gauges that need to be filtered out!
qcFinal = qcFinal[qcFinal["editRatio"] <= 12]
qcFinal = qcFinal.dropna(subset=["editRatio"])

# add column to track the error type
qcFinal["errorType"] = "Unknown"

# update unknown for unit errors
qcFinal.loc[qcFinal["editRatio"] >= 8, "errorType"] = "Unit"

# update unknown for meniscus errors
qcFinal.loc[(qcFinal["editDiff"] <= 3) & (qcFinal["editDiff"] > 0) & (qcFinal["editRatio"] < 8), "errorType"] = "Meniscus"

# summarize error statistics
numErrors = len(qcFinal)
pctErrors = round((len(qcFinal) / len(odk))*100,1)
numUnitErrors = qcFinal[qcFinal["errorType"] == "Unit"]["errorType"].count()
pctUnitErrors = round((numUnitErrors / numErrors) * 100,1)
numMenisErrors = qcFinal[qcFinal["errorType"] == "Meniscus"]["errorType"].count()
pctMenisErrors = round((numMenisErrors / numErrors) * 100,1)
numUnkErrors = qcFinal[qcFinal["errorType"] == "Unknown"]["errorType"].count()
pctUnkErrors = round((numUnkErrors / numErrors) * 100,1)

print("Number/percent of total errors are: {} and {} % of measurements!".format(numErrors, pctErrors))
print("Number/percent of unit errors are: {} and {} % of errors".format(numUnitErrors, pctUnitErrors))
print("Number/percent of meniscus errors are: {} and {} % of errors".format(numMenisErrors, pctMenisErrors))
print("Number/percent of unknown errors are: {} and {} % of errors".format(numUnkErrors, pctUnkErrors))

############################################### f1

nRows = 1
nColumns = 1

fig, ax = plt.subplots(nRows, nColumns, dpi=300, figsize=(12, 3))

qcFinal["editRatio"].dropna().plot.hist(bins=52, ax=ax)

ax.set_ylabel("Frequency", size=12)
ax.set_xlabel("Precipitation Edit Ratio ER (Edited Precip / Original Precip)", size=12)

plt.savefig(outputFilePath + '/' + f1, bbox_inches='tight')

fig.clf()

######################################  f2

nRows = 1
nColumns = 3

fig, ax = plt.subplots(nRows, nColumns, dpi=300, figsize=(12, 3.5))

fig.text(0.5, -0.04, "Original Precipitation (mm)", ha='center', size=16)
fig.text(0.07, 0.5, "Edited Precipitation (mm)", va='center', rotation='vertical', size=16)

subPlotTitles = ["0 - 10 mm", "0 - 50 mm", "0 - 200 mm"]

lim = [10, 50, 200]

x = qcFinal["precip_mm.old"]
y = qcFinal["precip_mm.new"]

i = 0

while i < (nRows * nColumns):

    subPlotID = int(str(nRows)+str(nColumns)+str(i+1))

    ax = plt.subplot(subPlotID)
    ax.add_patch(
        patches.Wedge(
        (0, 0),         # (x,y)
        250,            # radius
        82.89,             # theta1 (in degrees)
        85.24,            # theta2
        color="r", alpha=0.2
        )
    )
    ax.add_patch(
        patches.Rectangle(
        (0, -3),         # (x,y)
        300,            # width
        np.sin(np.pi/4)*3,             # height
        45,            # angle
        color="b", alpha=0.2
        )
    )
    ax = sns.scatterplot(x,y, label = "Observations", legend = False)
    ax.set_xlabel("")
    ax.set_ylabel("")
    ax.set_title(subPlotTitles[i], size=16)
    ax.set_xlim(0,lim[i])
    ax.set_ylim(0,lim[i])
    ax.tick_params(
        axis="both",
        which="major",
        labelsize=12)
    # for the last plot, force there to be 5 ticks at 40 mm intervals
    if i == 2:
        ax.xaxis.set_ticks(np.arange(0, 201, 40))
        ax.yaxis.set_ticks(np.arange(0, 201, 40))
    
    i = i + 1

plt.savefig(outputFilePath + '/' + f2, bbox_inches='tight')

fig.clf()

################################## f3

qcFinal["unknownError"] = qcFinal.loc[qcFinal["errorType"] == "Unknown"]["errorType"]
qcFinal["unitError"] = qcFinal.loc[qcFinal["errorType"] == "Unit"]["errorType"]
qcFinal["menisError"] = qcFinal.loc[qcFinal["errorType"] == "Meniscus"]["errorType"]

# calculate stats for errors by CS
qcMon = qcFinal.groupby(["MonID"]).agg({"errorType": "count",
                       "unknownError": "count",
                       "unitError": "count",
                       "menisError": "count",
                       "MonID": "mean"})

qcMon = qcMon.rename({"errorType": "numCor"}, axis="columns")
    
qcMonRec = odk.groupby(["MonID"]).agg({"precip_mm": "count",
                      "MonID": "mean"})
    
qcMonRec = qcMonRec.rename({"precip_mm": "numRec"}, axis="columns")

qcMonMerge = pd.merge(qcMonRec, qcMon, how="left", left_index=True, right_index=True).sort_values("numRec").fillna(0)

# delete unknown CS from the dataframe
qcMonMerge = qcMonMerge[qcMonMerge.index != 999]

qcMonMerge["allPct"] = 100

qcMonMerge["unitErrorPct"] = (qcMonMerge["unitError"] / qcMonMerge["numRec"])*100
qcMonMerge["menisErrorPct"] = (qcMonMerge["menisError"] / qcMonMerge["numRec"])*100
qcMonMerge["unknownErrorPct"] = (qcMonMerge["unknownError"] / qcMonMerge["numRec"])*100

qcMonMerge["unitMenisErrorPct"] = qcMonMerge["unitErrorPct"] + qcMonMerge["menisErrorPct"]
qcMonMerge["errorPct"] = qcMonMerge["unitErrorPct"] + qcMonMerge["menisErrorPct"] + qcMonMerge["unknownErrorPct"]
qcMonMerge["PR"] = 100 - qcMonMerge["errorPct"]
qcMonMerge["PRunknownErrorPct"] = qcMonMerge["PR"] + qcMonMerge["unknownErrorPct"]
qcMonMerge["PRunknownMenisErrorPct"] = qcMonMerge["PR"] + qcMonMerge["unknownErrorPct"] + qcMonMerge["menisErrorPct"]

qcMonMerge = qcMonMerge.sort_values("numRec", ascending=False)

# Economic analysis of costs per observation
###################################################################

# read in CS classification data
cs = pd.read_excel(inputFilePath + "/" + fileName3, sheet_name = "CS", header = 0)

# make list of columns to keep
colsToKeep = ["MonitorID", "numRec", "numCor", "PR", "RecruitMethod", "MonTypeID"]

# append CS class (paid vs volunteer) to costPerObs df
costPerObs = pd.merge(qcMonMerge, cs, how = "left", 
                      left_on = "MonID_x",
                      right_on = "MonitorID")[colsToKeep].reset_index()

# fill rows with nan for MonitorID, MonTypeID, and RecruitType
costPerObs["MonitorID"].fillna(999, inplace = True)
costPerObs["MonTypeID"].fillna("Volunteer", inplace = True)
costPerObs["RecruitMethod"].fillna("PersonalConnection", inplace = True)

# set key variables
cNumCS = len(costPerObs)
cNumR1 = len(costPerObs[costPerObs["RecruitMethod"] == "PersonalConnection"])
cNumR2 = len(costPerObs[costPerObs["RecruitMethod"] == "SocialMedia"])
cNumR3 = len(costPerObs[costPerObs["RecruitMethod"] == "Outreach"])
cNumR4 = len(costPerObs[costPerObs["RecruitMethod"] == "RandomVisit"])
cNumVolCS = len(costPerObs[costPerObs["MonTypeID"] == "Volunteer"])
cNumPaidCS = len(costPerObs[costPerObs["MonTypeID"] == "Paid"])
cNumWeeks = 22
cS4WGaugeCost = 100
cCoCoRaHSGaugeCost = 3600
cDHMGaugeCost = 7500
USDtoNPR = 114.34

# set which gauge you want to use
cGaugeCost = cS4WGaugeCost

# set per CS recruitement costs

# Using an hourly rate of 50 NPR, it was assumed that leveraging personal relationships
# took four staff 10 hours per week for two weeks, for a total of 4000 NPR.  Since 53 CS
# were recruited with this method, the cost was 75 NPR per CS recruited.
cR1 = 4*2*10*50 / cNumR1
# For social media, an investment of two hours per week at 50 NPR was made.  Since 11 CS
# were recruited with this method, the cost was 200 NPR per CS recruited.
cR2 = 22*2*50 / cNumR2
# Workshops and outreaches were organized at a total of four schools and five
# colleges/universities.  Workshops at schools and colleges/universities were estimated
# to cost 2500 and 5000 NPR, respectively.  Since 61 CS were recruited with this
# method, the cost was 574 NPR per CS.
cR3 = (4*2500 + 5*5000) / cNumR3
# Random site visits were used to recruite CS in rural areas. 
# Assuming a two person team, working for eight hours, for 50 NPR/hr, plus 40 km traveled per day, a
# daily subsistance allowance of 200 NPR/person, and recruitment of 5 CS per day, the 
# average cost was 280 NPR per CS.
cR4 = (2*50*8+40*5+200*2)/5

# set per CS motivation costs

# There are three types of personal follow ups: SMS (cM1a), phone calls (cM1b), and site visits (cM1cV and cM1cP)
# For SMS, we assumed that each CS received eight SMS messages during the monsoon, and that each message cost 10 NPR.
cM1a = 10*8
# For phone calls, we assumed that each CS received eight phone calls, and that each call cost 15 NPR.
cM1b = 15*8
# For site visits, we assumed that each volunteer CS received two site visits.
# Assuming a two person team, working for eight hours, for 50 NPR/hr, plus 40 km traveled per day, a
# daily subsistance allowance of 200 NPR/person, and visits of 10 CS per day, the 
# average cost was 140 NPR per CS per visit, or roughly 280 NPR per CS total.
cM1cV = 2*(2*50*8+40*5+200*2)/10
# Assuming a two person team, working for eight hours, for 50 NPR/hr, plus 40 km traveled per day, a
# daily subsistance allowance of 200 NPR/person, and visits of 10 CS per day, the 
# average cost was 140 NPR per CS per visit, or 700 NPR per CS total.
cM1cP = 5*(2*50*8+40*5+200*2)/10
# Bulk SMS messages were sent weekly, and cost roughly 3 NPR per message including 
# the time to generate and load the necessary report(s).
cM2 = 3 * cNumWeeks
# Outreach workshops focused on motivating Volunteer CS, at an estimated cost of 
# 40,000 NPR total, or with 117 volunteer CS, 342 NPR per volunteer CS.
cM3 = 40000 / cNumVolCS
# The motivation of data use was considered to have negligible cost, because of existing
# infrastructure necessary for other purposes.
cM4 = 0
# Lucky draws were used as a motivation for volunteer CS.  Nine lucky draws were performed,
# with an estimated cost of 1200 NPR each for 117 volunteers, or 92 NPR per volunteer CS.
cM5 = (1200*9) / cNumVolCS
# Certificates were used to motivate volunteer CS, and cost 25 NPR each.
cM6 = 25
# Payments were used to motivate paid CS, and cost 25 NPR per observation.
cM7 = 25

# solve for total motivational costs for volunteer and paid CS
# paid total doesn't include per observation payments
cMSumVol = cM1a + cM1b + cM1cV + cM2 + cM3 + cM4 + cM5 + cM6
cMSumPaid = cM1a + cM1b + cM1cP + cM2

# update costPerObs with equipment costs
costPerObs["EquipCostsNPR"] = cGaugeCost

# update costPerObs with recruitment costs
costPerObs.loc[costPerObs["RecruitMethod"] == "PersonalConnection", "RecCostsNPR"] = cR1
costPerObs.loc[costPerObs["RecruitMethod"] == "SocialMedia", "RecCostsNPR"] = cR2
costPerObs.loc[costPerObs["RecruitMethod"] == "Outreach", "RecCostsNPR"] = cR3
costPerObs.loc[costPerObs["RecruitMethod"] == "RandomVisit", "RecCostsNPR"] = cR4

# update costPerObs with motivation costs
costPerObs["MotCostsNPR"] = cMSumVol
costPerObs.loc[costPerObs["MonTypeID"] == "Paid", "MotCostsNPR"] = cMSumPaid

# this line adds in per observation payments for paid CS
costPerObs.loc[costPerObs["MonTypeID"] == "Paid", "MotCostsNPR"] = costPerObs["MotCostsNPR"] + costPerObs["numRec"] * cM7

costPerObs["TotalCostsNPR"] = costPerObs["EquipCostsNPR"] + costPerObs["RecCostsNPR"] + costPerObs["MotCostsNPR"]

costPerObs["CostPerObsNPR"] = costPerObs["TotalCostsNPR"] / costPerObs["numRec"]

costPerObs["CostPerObsUSD"] = costPerObs["CostPerObsNPR"] / USDtoNPR

import matplotlib.gridspec as gridspec
from matplotlib.patches import Ellipse

nRows = 4
nColumns = 2

fig, ax = plt.subplots(nRows, nColumns, dpi=300, figsize=(12, 6))
gridspec.GridSpec(nRows,nColumns)

# make top left subplot scatterplot
ax = plt.subplot2grid((4,2), (0,0), colspan=1, rowspan=3)
plt.subplots_adjust(hspace = 0.3)
x = costPerObs["CostPerObsUSD"]
y = costPerObs["PR"]
ax = sns.scatterplot(x, y, hue = costPerObs["MonTypeID"], legend = False)
#    ax.set_xscale("log")
#    ax.set_yscale("log")
ax.set_title("(a) Cost vs Performance", size=14)
ax.set_xlabel("")
ax.set_ylabel("Performance Ratio (%)", size=14)
ax.set_xlim(0,16)
ax.set_ylim(45,105)
ax.tick_params(
    axis='x',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom=False,      # ticks along the bottom edge are off
    top=False,         # ticks along the top edge are off
    labelbottom=False) # labels along the bottom edge are off

ells = Ellipse(xy=(12.4, 100), width=5.5, height=6, color="black", 
               alpha=0.8, fill=False)
ax.add_artist(ells)

ax.annotate("2 Measurements",
            xy=(5.16, 51), xycoords='data',
            xytext=(4.5, 75), textcoords='data',
            arrowprops=dict(arrowstyle="->",
                            connectionstyle="arc3",
                            color = "black")
            )
ax.annotate("",
            xy=(7.34, 99), xycoords='data',
            xytext=(6.8, 77.5), textcoords='data',
            arrowprops=dict(arrowstyle="->",
                            connectionstyle="arc3",
                            color = "black")
            )
ax.annotate("1 Measurement",
            xy=(12.7, 97), xycoords='data',
            xytext=(10, 85), textcoords='data',
            arrowprops=dict(arrowstyle="-",
                            connectionstyle="arc3",
                            color = "black")
            )

# add linear regression to first 

#costPerObsLessPR100 = costPerObs[costPerObs["PR"] != 100]
#x = costPerObsLessPR100["CostPerObsUSD"]
#y = costPerObsLessPR100["PR"]
#
#r = np.corrcoef(x, y)[0,1]
#r2 = r**2
#A = np.vstack([x, np.ones(len(x))]).T
#m, b = np.linalg.lstsq(A, y, rcond = None)[0]
#n = len(costPerObsLessPR100)
#
#ax.plot(x, m * x + b, label = "Linear Regression", 
#        linestyle = ":", color = "gray")
#
#ax.annotate("n = " + str(n), xy=(0.97, 0.23), xycoords="axes fraction", 
#            horizontalalignment="right", size = 12)
#ax.annotate("y = " + str(round(m,2)) + "x + " + str(round(b,1)), xy=(0.97, 0.17), 
#            xycoords="axes fraction", horizontalalignment="right", size = 12)
#ax.annotate("r = " + str(round(r,3)), xy=(0.97, 0.11), 
#            xycoords="axes fraction", horizontalalignment="right", size = 12)
#ax.annotate("r critical = 0.266 (p = 0.01)", xy=(0.97, 0.05), 
#            xycoords="axes fraction", horizontalalignment="right", size = 12)


# make bottom left subplot histogram
ax = plt.subplot2grid((4,2), (3,0), colspan=1, rowspan=1)
ax = sns.distplot(costPerObs["CostPerObsUSD"],
                  kde = False, 
                  bins = np.linspace(0, 16, num=64, endpoint=False),
                  color = "#4C72B0",
                  hist_kws=dict(alpha=1))
ax = sns.distplot(costPerObs.loc[costPerObs["MonTypeID"] == "Paid", "CostPerObsUSD"], 
                  kde = False, 
                  bins = np.linspace(0, 16, num=64, endpoint=False),
                  color = "#DD8452",
                  hist_kws=dict(alpha=1))
ax.set_title("(c) Histogram", size=14)
ax.set_xlabel("Average Cost per Observation (USD)", size=14)
ax.set_ylabel("Count", size=14)
ax.set_xlim(0,16)
 
# make top right subplot scatterplot
ax = plt.subplot2grid((4,2), (0,1), colspan=1, rowspan=3)
plt.subplots_adjust(hspace = 0.3)
ax = sns.scatterplot(costPerObs["CostPerObsUSD"],costPerObs["PR"],
                     hue = costPerObs["MonTypeID"],
                     legend = "brief")
#    ax.set_xscale("log")
#    ax.set_yscale("log")
ax.set_title("(b) Cost vs Performance (0-2 USD)", size=14)
ax.set_xlabel("")
ax.set_ylabel("Performance Ratio (%)", size=14)
ax.set_xlim(0,2)
ax.set_ylim(45,105)
ax.tick_params(
    axis='x',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom=False,      # ticks along the bottom edge are off
    top=False,         # ticks along the top edge are off
    labelbottom=False) # labels along the bottom edge are off

# make bottom right subplot histogram
ax = plt.subplot2grid((4,2), (3,1), colspan=1, rowspan=1)
ax = sns.distplot(costPerObs["CostPerObsUSD"],
                  kde = False, 
                  bins = np.linspace(0, 2, num=40, endpoint=False),
                  color = "#4C72B0",
                  hist_kws=dict(alpha=1))
ax = sns.distplot(costPerObs.loc[costPerObs["MonTypeID"] == "Paid", "CostPerObsUSD"], 
                  kde = False, 
                  bins = np.linspace(0, 2, num=40, endpoint=False),
                  color = "#DD8452",
                  hist_kws=dict(alpha=1))
ax.set_title("(d) Histogram (0-2 USD)", size=14)
ax.set_xlabel("Average Cost per Observation (USD)", size=14)
ax.set_ylabel("Count", size=14)
ax.set_xlim(0,2)

plt.savefig(outputFilePath + '/' + f4, bbox_inches='tight')

fig.clf()

costPerObs.loc[costPerObs["MonTypeID"] == "Volunteer", "CostPerObsUSD"].mean()
costPerObs.loc[costPerObs["MonTypeID"] == "Volunteer", "CostPerObsUSD"].median()
costPerObs.loc[costPerObs["MonTypeID"] == "Volunteer", "CostPerObsUSD"].min()
costPerObs.loc[costPerObs["MonTypeID"] == "Volunteer", "CostPerObsUSD"].max()
costPerObs.loc[costPerObs["MonTypeID"] == "Paid", "CostPerObsUSD"].mean()
costPerObs.loc[costPerObs["MonTypeID"] == "Paid", "CostPerObsUSD"].median()
costPerObs.loc[costPerObs["MonTypeID"] == "Paid", "CostPerObsUSD"].min()
costPerObs.loc[costPerObs["MonTypeID"] == "Paid", "CostPerObsUSD"].max()
costPerObs["CostPerObsUSD"].median()

costPerObsSummary = costPerObs.describe()

costList = [cR1, cR2, cR3, cR4, 
            cM1a, cM1b, cM1cP, cM1cV, cM2, cM3, cM4, cM5, cM6, cM7,
            cMSumVol, cMSumPaid]
costListIndex = ["R1_Personal", "R2_SocialMedia", "R3_Outreach", 
                 "R4_RandomVisit", "M1a_SMS", "M1b_Calls", 
                 "M1cP_SiteVisit", "M1cV_SiteVisit", "M2_SMS", 
                 "M3_Outreach", "M4_Data", "M5_LuckyDraw", 
                 "M6_Certificate", "M7_Payment",
                 "VolunteerSum", "PaidSum"]

costUnitSummary = pd.DataFrame(np.array(costList), columns = ["NPR"], index = costListIndex)
costUnitSummary["USD"] = round(costUnitSummary["NPR"]/USDtoNPR,2)

########################################################

cs_MonTypeID = cs.loc[:,["MonitorID", "MonTypeID"]]

qcMonMerge = pd.merge(qcMonMerge, cs_MonTypeID, how="left", left_on="MonID_x", right_on="MonitorID")
qcMonMerge["MonTypeID"].fillna("Volunteer", inplace = True)

# define dimensions of subplots
nRows = 2
nColumns = 1

# define fig and ax, resolution, and size in inches
fig, ax = plt.subplots(nRows, nColumns, dpi=300, figsize=(12, 8))

# use i to iterate through
i = 0

# define columns to plot along with titles
numRecPlot = ["numRec"]
errorPlot = ["allPct", "PRunknownMenisErrorPct", "PRunknownErrorPct", "PR"]
Labels = ["Unit Errors", "Meniscus Errors", "Unknown Errors", "Performance Ratio (PR)"]
colors = ["#d7191c", "#fdae61", "#2c7bb6", "lightsteelblue"]
subplotTitles = ["(a) Number of Measurements", "(b) Error Composition"]

while i < (nRows * nColumns):
    
    # make subplot boxplot with Seaborn
    ax = plt.subplot(nRows, nColumns, i+1)
    if i == 0:
        for j in range(len(numRecPlot)):
            ax = sns.barplot(x="MonID_x", y=numRecPlot[j], data=qcMonMerge, 
                             order=qcMonMerge["MonID_x"], 
                             palette=["dodgerblue", "#DD8452"], 
                             hue="MonTypeID",
                             dodge=False)
        ax.tick_params(
                axis='x',          # changes apply to the x-axis
                which='both',      # both major and minor ticks are affected
                bottom=False,      # ticks along the bottom edge are off
                top=False,         # ticks along the top edge are off
                labelbottom=False) # labels along the bottom edge are off
        ax.tick_params(
                axis="both",
                which="major",
                labelsize=12)
        ax.set_xlabel("Citizen Scientists (n = 154)", size = 14)
        ax.set_ylabel("Number of Measurements", size = 14)
        ax.legend(loc=1, frameon=True, framealpha=0.8, prop={'size': 12})

    if i == 1:
        for j in range(len(errorPlot)):
            ax = sns.barplot(x="MonID_x", y=errorPlot[j], data=qcMonMerge, 
                             order=qcMonMerge["MonID_x"], color=colors[j], label = Labels[j])
        ax.legend(loc=4, frameon=True, framealpha=0.8, prop={'size': 12})
        ax.tick_params(
                axis='x',          # changes apply to the x-axis
                which='both',      # both major and minor ticks are affected
                bottom=False,      # ticks along the bottom edge are off
                top=False,         # ticks along the top edge are off
                labelbottom=False) # labels along the bottom edge are off
        ax.set_xlabel("Citizen Scientists (n = 154)", size = 14)
        ax.set_ylabel("% of Measurements", size = 14)
        ax.set_ylim(0,100)
        ax.tick_params(
                axis="both",
                which="major",
                labelsize=12)
    # set subplot titles, limits, and labels
    ax.set_title(subplotTitles[i], size = 16)
    
    i = i + 1

# save figure
plt.savefig(outputFilePath + '/' + f3, bbox_inches='tight')

# clear figure
fig.clf()

qcStatsAllErrors = len(qcMonMerge[(qcMonMerge["unknownError"] != 0) & (qcMonMerge["unitError"] != 0) & (qcMonMerge["menisError"] != 0)])
qcStatsUnitMenisErrors = len(qcMonMerge[(qcMonMerge["unitError"] != 0) & (qcMonMerge["menisError"] != 0)])
qcStatsUnitUnknownErrors = len(qcMonMerge[(qcMonMerge["unitError"] != 0) & (qcMonMerge["unknownError"] != 0)])
qcStatsMenisUnknownErrors = len(qcMonMerge[(qcMonMerge["menisError"] != 0) & (qcMonMerge["unknownError"] != 0)])

qcMonMerge["numErrorSum"] = qcMonMerge["unitError"] + qcMonMerge["menisError"] + qcMonMerge["unknownError"]

meanPR = qcMonMerge["PR"].mean()

# perform Mann-Whitney U test on performance ratio (PR)

q1 = np.percentile(qcMonMerge["numRec"],25)
q2 = np.percentile(qcMonMerge["numRec"],50)
q3 = np.percentile(qcMonMerge["numRec"],75)

qcMonMergeIQR = qcMonMerge.loc[(qcMonMerge["numRec"] >= q1) & (qcMonMerge["numRec"] <= q3)]
qcMonMergeOQR = qcMonMerge.loc[(qcMonMerge["numRec"] < q1) | (qcMonMerge["numRec"] > q3)]
qcMonMergeL50 = qcMonMerge.loc[(qcMonMerge["numRec"] <= q2) & (qcMonMerge["PR"] == 100)]
qcMonMergeU50 = qcMonMerge.loc[(qcMonMerge["numRec"] > q2) & (qcMonMerge["PR"] == 100)]
qcMonMergeLQ1 = qcMonMerge.loc[(qcMonMerge["numRec"] < q1) & (qcMonMerge["PR"] == 100)]

print("Number of 100 % PR at or below median numRec: {}".format(qcMonMergeL50.PR.count()))
print("Number of 100 % PR at or above median numRec: {}".format(qcMonMergeU50.PR.count()))
print("Number of 100 % PR at or below Q1 numRec: {}".format(qcMonMergeLQ1.PR.count()))

U_pr = scipy.stats.mannwhitneyu(qcMonMergeIQR["PR"], 
                                    qcMonMergeOQR["PR"], 
                                    use_continuity=True, alternative="less")
