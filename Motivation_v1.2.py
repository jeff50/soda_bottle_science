# -*- coding: utf-8 -*-
"""
Created on Tue Sep  4 23:36:43 2018

@author: Jeff2017
"""

# Author: Jeff Davids
# Modified: 
# Company: TU Delft
# Date: 180804
# Date Modified:
# Purpose: Create array of line plots
# v1.0
#
# Instructions: modify file paths, etc. and run
#
#   Import system modules

import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy
import matplotlib.ticker as ticker
import matplotlib.patches as mpatches
import seaborn as sns
from datetime import datetime
from pandas import ExcelWriter
from matplotlib import pyplot

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 800)

# coding: utf-8

# define variables

mpl.style.use("seaborn")

inputFilePath = r"C:\_MyDocs\OneDrive\_My_Education\PhD_TUDelft\Docs\Publications\4_CS_Precipitation\Data\Input"
outputFilePath = r"C:\_MyDocs\OneDrive\_My_Education\PhD_TUDelft\Docs\Publications\4_CS_Precipitation\Data\Output"
fileName1 = "S4W-Nepal_Precip_Data.xlsx"
fileName2 = "S4W-Nepal_CS_Classifications.xlsx"

df = pd.read_excel(inputFilePath + "/" + fileName1, sheet_name = "Sheet1", header = 0)
df = df[df["precip_mm"]<=200]
df["datetime"] = pd.to_datetime(df["msmt_datetime"])
df["year-month"] = df["datetime"].dt.strftime("%Y-%m")
df["year-week"] = df["datetime"].dt.strftime("%Y-%W")
df["week"] = df["datetime"].dt.week
df["month"] = df["datetime"].dt.month
df["year"] = df["datetime"].dt.year
df = df[df["MonID"]!="-1_Unknown"]
df = df[df["year"]>=2018]
df = df[df["month"]>=5]
df = df[df["month"]<10]
df["MonitorID"] = df["MonID"].str.split("_").str[1]
df["MonitorID"] = df["MonitorID"].apply(int)
df.sort_values(by=["datetime"])

df = df.groupby(["MonitorID", "year-month"])["precip_mm"].count()
df = df.reset_index()

#set all values greater than 31 to 31
df.loc[df["precip_mm"] > 31, "precip_mm"] = 31
df.sort_values(by=["year-month"])
             
df_cs = pd.read_excel(inputFilePath + "/" + fileName2, sheet_name = "CS", header = 0)

df_cs = df_cs.replace({"Staff": "Volunteer", "paid": "Paid"})

df_cs = df_cs[["MonitorID", "MonTypeID", "Age", "EducationLevel", "Occupation", "RuralUrban", "Gender"]]

df_merge = pd.merge(df, df_cs, how="outer", on="MonitorID")
df_merge = df_merge.sort_values(by=["year-month"])
df_merge = df_merge.rename({"precip_mm": "Msmts per CS per Month",
                 "year-month": "Year-Month"}, axis="columns")
df_merge = df_merge.dropna(subset=["Msmts per CS per Month"])

df_merge["Age"] = pd.Categorical(df_merge["Age"], ["<=18", "19-25", ">25"])
df_merge = df_merge.sort_values(["Age"])

# set values over 30 for June and September to 30
df_merge.loc[(df_merge["Year-Month"] == "2018-06") & (df_merge["Msmts per CS per Month"] > 30), 
             "Msmts per CS per Month"] = 30
df_merge.loc[(df_merge["Year-Month"] == "2018-09") & (df_merge["Msmts per CS per Month"] > 30), 
             "Msmts per CS per Month"] = 30

nRows = 2
nColumns = 2

fig, ax = plt.subplots(nRows, nColumns, dpi=300, figsize=(15, 10))

fig.text(0.5, 0.07, "Year-Month", ha='center', size = 16)
fig.text(0.08, 0.5, "Measurements Per Citizen Scientist Per Month", va='center', rotation='vertical', size = 16)

hueList = ["MonTypeID", "Gender", "Age", "EducationLevel"]

figTitles = ["Volunteer or Paid", "Gender", "Age", "Education"]

# "Age", "EducationLevel", "Occupation", "RuralUrban", "MonTypeID", "Gender"

MonTypeIDCount = df_merge.groupby(["MonTypeID", "Year-Month"])["MonitorID"].count().reset_index()

i = 0

while i < (nRows * nColumns):
    
    df_merge.sort_values("Gender")
    ax = plt.subplot(nRows, nColumns, i+1)
    box = sns.boxplot(x="Year-Month", y="Msmts per CS per Month", 
                hue=hueList[i], 
                data=df_merge,
                palette="Blues")
    box.tick_params(labelsize=16)
    ax.legend(loc=2, frameon=True, framealpha=0.8, prop={'size': 16})

# To control the actual list of values in legends
#    handles, _ = box.get_legend_handles_labels()
#    box.legend(handles, ["Man", "Woman"])

    ax.set_title(figTitles[i], size=16)
    ax.set_ylim(0, 31)
    ax.set_ylabel("")    
    ax.set_xlabel("")
#    if i < ((nRows * nColumns) - 2):
#        ax.set_xticklabels("")
    i = i + 1

plt.savefig(outputFilePath + '/' + 'Fig7_CS_Motivation.tiff', bbox_inches='tight')

fig.clf()

###########################
# This code makes a historgram of total measurements

df_cs_msmts = df_merge.groupby(["MonitorID"]).agg({"Msmts per CS per Month": "sum"})
df_cs_msmts["msmts"] = df_cs_msmts["Msmts per CS per Month"]

nRows = 1
nColumns = 1

fig, ax = plt.subplots(nRows, nColumns, dpi=300, figsize=(12, 2))

df_cs_msmts["msmts"].plot.hist(bins=147, ax=ax)

ax.set_ylabel("Frequency", size=12)
ax.set_xlabel("Total Number of Measurements by Citizen Scientist", size=12)

ax.annotate("n = " + str(round(df_cs_msmts["msmts"].count(),1)), 
            xy=(0.98, 0.9), xycoords="axes fraction", horizontalalignment="right")
ax.annotate("mean = " + str(round(df_cs_msmts["msmts"].mean(),1)), 
            xy=(0.98, 0.8), xycoords="axes fraction", horizontalalignment="right")
ax.annotate("min = " + str(round(df_cs_msmts["msmts"].min(),1)), 
            xy=(0.98, 0.7), xycoords="axes fraction", horizontalalignment="right")
ax.annotate("max = " + str(round(df_cs_msmts["msmts"].max(),1)), 
            xy=(0.98, 0.6), xycoords="axes fraction", horizontalalignment="right")
ax.annotate("stdev = " + str(round(df_cs_msmts["msmts"].std(),1)), 
            xy=(0.98, 0.5), xycoords="axes fraction", horizontalalignment="right")

plt.savefig(outputFilePath + '/' + 'CS_Motivation_Msmts_Per_Week.png', bbox_inches='tight')

fig.clf()

#############################
# Mann Whtney U Tests

U_gender = scipy.stats.mannwhitneyu(df_merge.loc[df_merge["Gender"] == "Female", "Msmts per CS per Month"],
                         df_merge.loc[df_merge["Gender"] == "Male", "Msmts per CS per Month"],
                         use_continuity=True, alternative="greater")

U_type = scipy.stats.mannwhitneyu(df_merge.loc[df_merge["MonTypeID"] == "Paid", "Msmts per CS per Month"],
                         df_merge.loc[df_merge["MonTypeID"] == "Volunteer", "Msmts per CS per Month"],
                         use_continuity=True, alternative="greater")

U_lt18 = scipy.stats.mannwhitneyu(df_merge.loc[df_merge["Age"] == "<=18", "Msmts per CS per Month"],
                         df_merge.loc[(df_merge["Age"] == "19-25") | (df_merge["Age"] == ">25")
                         , "Msmts per CS per Month"],
                         use_continuity=True, alternative="greater")

U_19to25 = scipy.stats.mannwhitneyu(df_merge.loc[df_merge["Age"] == "19-25", "Msmts per CS per Month"],
                         df_merge.loc[(df_merge["Age"] == "<=18") | (df_merge["Age"] == ">25")
                         , "Msmts per CS per Month"],
                         use_continuity=True, alternative="greater")
    
U_gt25 = scipy.stats.mannwhitneyu(df_merge.loc[df_merge["Age"] == ">25", "Msmts per CS per Month"],
                         df_merge.loc[(df_merge["Age"] == "<=18") | (df_merge["Age"] == "19-25")
                         , "Msmts per CS per Month"],
                         use_continuity=True, alternative="greater")

U_ltBSc = scipy.stats.mannwhitneyu(df_merge.loc[df_merge["EducationLevel"] == "<Bachelors", "Msmts per CS per Month"],
                         df_merge.loc[(df_merge["EducationLevel"] == "Bachelors") | (df_merge["EducationLevel"] == ">Bachelors")
                         , "Msmts per CS per Month"],
                         use_continuity=True, alternative="greater")

U_BSc = scipy.stats.mannwhitneyu(df_merge.loc[df_merge["EducationLevel"] == "Bachelors", "Msmts per CS per Month"],
                         df_merge.loc[(df_merge["EducationLevel"] == "<Bachelors") | (df_merge["EducationLevel"] == ">Bachelors")
                         , "Msmts per CS per Month"],
                         use_continuity=True, alternative="greater")

U_gtBSc = scipy.stats.mannwhitneyu(df_merge.loc[df_merge["EducationLevel"] == ">Bachelors", "Msmts per CS per Month"],
                         df_merge.loc[(df_merge["EducationLevel"] == "<Bachelors") | (df_merge["EducationLevel"] == "Bachelors")
                         , "Msmts per CS per Month"],
                         use_continuity=True, alternative="greater")