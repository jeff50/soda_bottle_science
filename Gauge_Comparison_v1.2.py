# -*- coding: utf-8 -*-
"""
Created on Thu Sep  6 22:31:06 2018

@author: Jeff2017
"""

import pandas as pd
import numpy as np
import seaborn as sns; sns.set()
import matplotlib as mpl
import matplotlib.pyplot as plt
from datetime import datetime
from dateutil.parser import parse
from datetime import timedelta
import os

inputFilePath = r"C:\_MyDocs\OneDrive\_My_Education\PhD_TUDelft\Docs\Publications\4_CS_Precipitation\Data\Input"
outputFilePath = r"C:\_MyDocs\OneDrive\_My_Education\PhD_TUDelft\Docs\Publications\4_CS_Precipitation\Data\Output"

f1 = "Fig5_Gauge_Comparison.tiff"
f2 = "SMFig1_S4W_Corrected.png"
f3 = "Gauge_Sums.png"

dirList = os.listdir(inputFilePath)

# read and parse Onset files
onset = pd.DataFrame()
list_ = []
for file in dirList:
    siteID = file.split("_")[0]
    if ".csv" in file:
        df = pd.read_csv(inputFilePath + "/" + file, header=None, index_col=False, 
                         skiprows=2, names=["recNum", "datetime", "temp_c", 
                                            "onset_mm", "shut_attach", 
                                            "log_stop", "end_file"])
        df["siteID"] = siteID
        list_.append(df)

onset = pd.concat(list_)
onset = onset.drop_duplicates()
onset = onset.set_index(["siteID", "datetime"], drop=False, verify_integrity=True)

onset = onset[onset["onset_mm"] >= 0]
onset["datetime"] = pd.to_datetime(onset["datetime"])

# read in manually recorded gauge data
fileName = "S4W-Nepal_Gauge_Comparison.xlsx"

# set DHM column to either "dhm_mm" or "dhm_mm_2"
dhm = "dhm_mm_2"

manual = pd.read_excel(inputFilePath + "/" + fileName, sheet_name = "Sheet1", header = 0)
manual["msmt_datetime"] = pd.to_datetime(manual["msmt_datetime"])
manual["msmt_datetime"] = manual["msmt_datetime"] + timedelta(hours=5.75)
manual.sort_values(by=["msmt_datetime"])
manual.reset_index()

mpl.style.use("seaborn")

# compute Onset totals between each manually recorded interval
i = 0

onset_ = pd.DataFrame([])

while i < (len(manual)-1):
    temp = onset[(onset["datetime"] >= manual["msmt_datetime"].iloc[i]) & 
                 (onset["datetime"] < manual["msmt_datetime"].iloc[i+1])]
    onset_ = onset_.append(pd.DataFrame({"msmt_datetime": manual["msmt_datetime"].iloc[i+1], 
                                         "onset_mm": round(temp["onset_mm"].count()*0.2,1)}, index=[0]))
    i = i + 1
    
allData = pd.merge(manual, onset_, how="outer", on="msmt_datetime")

# remove partial data files from onset_mm
allData.loc[(allData["msmt_datetime"] <= pd.to_datetime("2017-06-06 18:28:00")), "onset_mm"] = np.nan
allData.loc[(allData["msmt_datetime"] >= pd.to_datetime("2018-01-30 16:51:00")), "onset_mm"] = np.nan

#calculate time difference between records in decimal days
allData["deltaT"] = (allData["msmt_datetime"] - allData["msmt_datetime"].shift()).fillna(0)
allData["deltaTDays"] = list(map(lambda dt: dt.total_seconds() / timedelta(days = 1).total_seconds(), allData["deltaT"]))

################################### f1

nRows = 3
nColumns = 3

fig, ax = plt.subplots(nRows, nColumns, dpi=300, figsize=(12, 12))

fig.text(0.5, 0.065, "DHM Precipitation (mm)", ha='center', size = 16)
fig.text(0.08, 0.5, "Evaluated Gauge Precipitation (mm)", va='center', rotation='vertical', size = 16)

yVals = ["s4w_mm", "s4w_mm", "s4w_mm", "cocorahs_mm", "cocorahs_mm", 
         "cocorahs_mm", "onset_mm", "onset_mm", "onset_mm"]
subPlotTitles = ["S4W (0-5)", "S4W (0-25)", "S4W (0-100)", "CoCoRaHS (0-5)", "CoCoRaHS (0-25)", 
                 "CoCoRaHS (0-100)", "Onset (0-5)", "Onset (0-25)", "Onset (0-100)"]

lim = [5, 25, 100, 5, 25, 100, 5, 25, 100]

i = 0

while i < (nRows * nColumns):

    subPlotID = int(str(nRows)+str(nColumns)+str(i+1))
    xyData = allData[[dhm, yVals[i]]].dropna()
    xyData = xyData[xyData[dhm]<=lim[i]]
    x = xyData[dhm]
    y = xyData[yVals[i]]

    ax = plt.subplot(subPlotID)
    ax = sns.scatterplot(x,y, label = "Observations", alpha=0.75)
    ax.set_title(subPlotTitles[i], size = 14)
    ax.set_xlim(0,lim[i])
    ax.set_ylim(0,lim[i])
    ax.set_xlabel("")    
    ax.set_ylabel("")
    
    r = np.corrcoef(x, y)[0,1]
    r2 = r**2
    n = len(x)

    x = x[:,np.newaxis]
    m, _, _, _ = np.linalg.lstsq(x, y, rcond=-1)
    ax.plot([0,lim[i]],[0,lim[i]*m], label = "Linear Regression", 
            linestyle = ":", color = "gray")

    ax.legend(loc = 2)
    
    ax.annotate("n = " + str(n), xy=(0.95, 0.18), xycoords="axes fraction", 
                horizontalalignment="right")
    ax.annotate("y = " + str(round(m[0],2)) + "x", xy=(0.95, 0.11), 
                xycoords="axes fraction", horizontalalignment="right")
    ax.annotate("R-Squared = " + str(round(r2,3)), xy=(0.95, 0.04), 
                xycoords="axes fraction", horizontalalignment="right")
    
    i = i + 1

plt.savefig(outputFilePath + '/' + f1, bbox_inches='tight')

#################################### f2

# Make plots of just corrected S4W Data!

# add data correction to S4W data
allData["s4w_corr_ecf_mm"] = allData["s4w_mm"]*1.03


# Initialize corrected data to original data
allData["s4w_corr_evap_mm"] = allData["s4w_mm"]

# For non-zero readings, add an offset to account for evaporation
allData.loc[(allData["s4w_mm"] > 0), "s4w_corr_evap_mm"] = (allData["s4w_mm"] + 0.5)
allData.loc[(allData["s4w_mm"] > 0), "s4w_corr_evap_mm"] = (allData["s4w_mm"] + 0.5)

# this is for the more complex correction that depends on the period of time between measurements
allData.loc[(allData["s4w_mm"] > 0) & (allData["deltaTDays"]<=1.6), "s4w_corr_evap_mm"] = (allData["s4w_mm"] + round(0.5 * allData["deltaTDays"],1))
allData.loc[(allData["s4w_mm"] > 0) & (allData["deltaTDays"]>1.6), "s4w_corr_evap_mm"] = (allData["s4w_mm"] + 0.8)

# Make cumulative sums
sumCols = ["s4w_mm", "cocorahs_mm", "onset_mm", dhm, "s4w_corr_ecf_mm", "s4w_corr_evap_mm"]

for sumCol in sumCols:
    newColName = sumCol + "_sum"
    allData[newColName] = allData[sumCol].cumsum()
    
allData["onset_mm_sum"] = allData["onset_mm_sum"] + 133.5

allData.set_index("msmt_datetime", drop=False, inplace=True)

nRows = 2
nColumns = 3

fig, ax = plt.subplots(nRows, nColumns, dpi=300, figsize=(12, 8))

fig.text(0.5, 0.06, "DHM Precipitation (mm)", ha='center', size = 16)
fig.text(0.08, 0.5, "Corrected S4W Readings (mm)", va='center', rotation='vertical', size = 16)

yVals = ["s4w_corr_ecf_mm", "s4w_corr_ecf_mm", "s4w_corr_ecf_mm", 
         "s4w_corr_evap_mm", "s4w_corr_evap_mm", "s4w_corr_evap_mm"]
subPlotTitles = ["(A) ECF (0-5)", "(B) ECF (0-25)", "(C) ECF (0-100)",
                 "(D) EVAP (0-5)", "(E) EVAP (0-25)", "(F) EVAP (0-100)"]

lim = [5, 25, 100, 5, 25, 100]

i = 0

while i < (nRows * nColumns):

    subPlotID = int(str(nRows)+str(nColumns)+str(i+1))
    xyData = allData[[dhm, yVals[i]]].dropna()
    xyData = xyData[xyData[dhm]<=lim[i]]
    x = xyData[dhm]
    y = xyData[yVals[i]]

    ax = plt.subplot(subPlotID)
    ax = sns.scatterplot(x,y, label = "Observations", alpha=0.75)
    ax.set_title(subPlotTitles[i], size = 16)
    ax.set_xlim(0,lim[i])
    ax.set_ylim(0,lim[i])
    ax.set_xlabel("")    
    ax.set_ylabel("")
    
    r = np.corrcoef(x, y)[0,1]
    r2 = r**2
    n = len(x)

    x = x[:,np.newaxis]
    m, _, _, _ = np.linalg.lstsq(x, y, rcond=-1)
    ax.plot([0,lim[i]],[0,lim[i]*m], label = "Linear Regression", 
            linestyle = ":", color = "gray")

    ax.legend(loc = 2)
    
    ax.annotate("n = " + str(n), xy=(0.95, 0.18), xycoords="axes fraction", 
                horizontalalignment="right")
    ax.annotate("y = " + str(round(m[0],2)) + "x", xy=(0.95, 0.11), 
                xycoords="axes fraction", horizontalalignment="right")
    ax.annotate("R-Squared = " + str(round(r2,3)), xy=(0.95, 0.04), 
                xycoords="axes fraction", horizontalalignment="right")
    
    i = i + 1

plt.savefig(outputFilePath + '/' + f2, bbox_inches='tight')

############################# f3

nRows = 1
nColumns = 1

fig, ax = plt.subplots(nRows, nColumns, dpi=300, figsize=(12, 4))

fig.text(0.5, 0.00, "Date (year-month)", ha='center')
fig.text(0.08, 0.5, "Cumulative Precipitation (mm)", va='center', 
         rotation='vertical')

toPlot = ["s4w_mm_sum", "cocorahs_mm_sum", "onset_mm_sum",  
          "dhm_mm_2_sum"]

# "s4w_corr_t106_mm_sum", "s4w_corr_p080_mm_sum"

legItems = ["S4W (mm)", "CoCoRaHS (mm)", "Onset (mm)",  
            "DHM (mm)"]

# "S4W Plus_0.86 (mm)", "S4W Times_1.06"

i = 0
for plot in toPlot:
    plt.plot(allData[plot], label=legItems[i])
    i = i + 1

plt.legend(loc = 2)

plt.savefig(outputFilePath + '/' + f3, bbox_inches='tight')

fig.clf()

######################### summary stats

dhm_sum = dhm + "_sum"
sumDHM = allData[dhm_sum].max()
sumCoCoRaHS = allData["cocorahs_mm_sum"].max()
errorCoCoRaHS = round(100*((sumCoCoRaHS - sumDHM) / sumDHM),1)
print("DHM Total = {} mm".format(round(sumDHM,1)))
print("CoCoRaHS Total = {} mm".format(round(sumCoCoRaHS,1)))
print("CoCoRaHS error = {} %".format(errorCoCoRaHS))

sumS4W = allData["s4w_mm_sum"].max()
errorS4W = round(100*((sumS4W - sumDHM) / sumDHM),1)

print("S4W Total = {} mm".format(round(sumS4W,1)))
print("S4W error = {} %".format(errorS4W))

sumS4Wecf = allData["s4w_corr_ecf_mm_sum"].max()
errorS4Wecf = round(100*((sumS4Wecf - sumDHM) / sumDHM),1)

print("S4W ECF Total = {} mm".format(round(sumS4Wecf,1)))
print("S4W ECF error = {} %".format(errorS4Wecf))

sumS4Wevap = allData["s4w_corr_evap_mm_sum"].max()
errorS4Wevap = round(100*((sumS4Wevap - sumDHM) / sumDHM),1)

print("S4W EVAP Total = {} mm".format(round(sumS4Wevap,1)))
print("S4W EVAP error = {} %".format(errorS4Wevap))