# -*- coding: utf-8 -*-
"""
Created on Tue Sep  4 23:36:43 2018

@author: Jeff2017
"""

# Author: Jeff Davids
# Modified: 
# Company: TU Delft
# Date: 180804
# Date Modified:
# Purpose: Create array of line plots
# v1.0
#
# Instructions: modify file paths, etc. and run
#
#   Import system modules

import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import scipy
import matplotlib.patches as mpatches
import seaborn as sns
from datetime import datetime
from pandas import ExcelWriter
from matplotlib import pyplot
import matplotlib.gridspec as gridspec

# use the 'seaborn-colorblind' style
plt.style.use('seaborn-colorblind')

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 800)

# coding: utf-8

# define variables

mpl.style.use("seaborn")

inputFilePath = r"C:\_MyDocs\OneDrive\_My_Education\PhD_TUDelft\Docs\Publications\4_CS_Precipitation\Data\Input"
outputFilePath = r"C:\_MyDocs\OneDrive\_My_Education\PhD_TUDelft\Docs\Publications\4_CS_Precipitation\Data\Output"
fileName = "S4W-Nepal_Precip_Data.xlsx"
fileName2 = "S4W-Nepal_CS_Classifications.xlsx"

df = pd.read_excel(inputFilePath + "/" + fileName, sheet_name = "Sheet1", header = 0)
df = df[df["precip_mm"]<=200]
df["datetime"] = pd.to_datetime(df["msmt_datetime"])
df["year-month"] = df["datetime"].dt.strftime("%Y-%m")
df["year-week"] = df["datetime"].dt.strftime("%Y-%W")
df["week"] = df["datetime"].dt.week
df["month"] = df["datetime"].dt.month
df["year"] = df["datetime"].dt.year
df["date"] = df["datetime"].dt.date
df["dayOfWeek"] = df["datetime"].dt.day_name()
df = df[df["MonID"]!="-1_Unknown"]
df = df[df["year"]>=2018]
df = df[df["month"]>=5]
df = df[df["month"]<10]
df.sort_values(by=["datetime"])

df_stats = df.groupby(["MonID"])["datetime"].count()
print("Mean: " + str(round(df_stats.mean(),2)))
print("Min: " + str(round(df_stats.min(),2)))
print("Max: " + str(round(df_stats.max(),2)))
print("Stdev: " + str(round(df_stats.std(),2)))
print("Number of single measurements: " + str(df_stats.where(df_stats == 1).count()))

df_sort = df.groupby(["MonID"])["datetime"].min()
df_sort = df_sort.reset_index()
df_sort = df_sort.sort_values("datetime")

# read in CS classification data
cs = pd.read_excel(inputFilePath + "/" + fileName2, sheet_name = "CS", header = 0)
cs = cs.loc[:,["MonitorID", "RecruitMethod", "MonTypeID"]]

pivot = pd.pivot_table(df, "precip_mm", "MonID", "year-week", aggfunc = "count")

pivot_sort = pd.merge(df_sort, pivot, how="outer", on="MonID")
pivot_sort["MonID_l"] = pivot_sort["MonID"].str.split("_", expand=True)[1].astype(int)
pivot_sort = pd.merge(pivot_sort, cs, how = "left", 
                      left_on = "MonID_l",
                      right_on = "MonitorID")
pivot_sort = pivot_sort.set_index(["MonID"])

heatmap = pivot_sort.drop(columns=["datetime", "MonID_l", "MonitorID", "RecruitMethod", "MonTypeID"])

heatmap_zero = pivot_sort.fillna(0)

heatmap_recsum = heatmap_zero.groupby("RecruitMethod").mean().drop(columns=["MonID_l", "MonitorID"])

heatmap_montype = heatmap_zero.groupby("MonTypeID").mean().drop(columns=["MonID_l", "MonitorID"])

# add column and sort by it and set it as index
heatmap_recsum["Recruitment Method"] = ["R3", "R1", "R4", "R2"]
heatmap_recsum = heatmap_recsum.set_index(["Recruitment Method"]).sort_index()

junk = len(pivot_sort[pivot_sort["MonTypeID"] == "Paid"])

# start making plots

nRows = 40
nColumns = 1

fig, ax = plt.subplots(nRows, nColumns, dpi=300, figsize=(16, 10))
gs1 = gridspec.GridSpec(nRows, nColumns)

gs1.update(wspace=1, hspace=1)

# make first heatmap
ax1 = plt.subplot2grid((nRows, nColumns), (0,0), colspan=1, rowspan=26)

sns.heatmap(heatmap, cmap="Blues", vmin=1, vmax=7, center=3.5, 
                    mask=heatmap.isnull(),
                    cbar=False)
ax1.patch.set_facecolor("white")

ax1.set_title("All Citizen Scientists", size=16)
ax1.set_xlabel("")
ax1.set_xticklabels("")
ax1.set_ylabel("")
ax1.set_yticklabels("")

ax = plt.subplot2grid((nRows, nColumns), (26,0), colspan=1, rowspan=3)
ax.patch.set_facecolor("white")
ax.set_xticklabels("")
ax.set_yticklabels("")

# add annotation to the color bar
ax.annotate("Number of Measurements Per Week", (0.973,0.47), size=12, horizontalalignment="right")

# make recruitment heatmap
ax2 = plt.subplot2grid((nRows, nColumns), (29,0), colspan=1, rowspan=6)

sns.heatmap(heatmap_recsum, cmap="Blues", vmin=1, vmax=7, center=3.5, 
                    mask=heatmap_recsum.isnull(),
                    cbar=True,
                    cbar_kws = dict(use_gridspec=False,location="top", anchor=(1,1)))
ax2.patch.set_facecolor("white")
ax2.set_title("Average by Recruitment Method", size=16)
ax2.set_xlabel("Year-Week", size=16)
ax2.set_ylabel("")
ax2.tick_params(axis="y", labelsize=12, rotation=0)
ax2.set_xticklabels("")

ax = plt.subplot2grid((nRows, nColumns), (35,0), colspan=1, rowspan=2)
ax.patch.set_facecolor("white")
ax.set_xticklabels("")
ax.set_yticklabels("")

# make motivation heatmap
ax3 = plt.subplot2grid((nRows, nColumns), (37,0), colspan=1, rowspan=3)

sns.heatmap(heatmap_montype, cmap="Blues", vmin=1, vmax=7, center=3.5, 
                    mask=heatmap_montype.isnull(),
                    cbar=False)
ax3.patch.set_facecolor("white")

ax3.set_title("Average by Motivation Method", size=16)
ax3.set_xlabel("Year-Week", size=16)
ax3.set_ylabel("")
ax3.tick_params(axis="x", labelsize=12, rotation=30)
ax3.tick_params(axis="y", labelsize=12, rotation=0)

plt.savefig(outputFilePath + '/' + 'Fig6_CS_Heatmap.tiff', bbox_inches='tight')

fig.clf()

#############################
# Mann Whtney U Tests

x = heatmap_montype.T["Paid"]
y = heatmap_montype.T["Volunteer"]

U_paid = scipy.stats.mannwhitneyu(x, y, use_continuity=True, alternative="greater")

x = heatmap_recsum.T["R1"]
y = heatmap_recsum.T["R2"]

U_r1_r2 = scipy.stats.mannwhitneyu(x, y, use_continuity=True, alternative="less")

x = heatmap_recsum.T["R1"]
y = heatmap_recsum.T["R4"]

U_r1_r4 = scipy.stats.mannwhitneyu(x, y, use_continuity=True, alternative="less")

x = heatmap_recsum.T["R3"]
y = heatmap_recsum.T["R2"]

U_r3_r2 = scipy.stats.mannwhitneyu(x, y, use_continuity=True, alternative="less")

x = heatmap_recsum.T["R3"]
y = heatmap_recsum.T["R4"]

U_r3_r4 = scipy.stats.mannwhitneyu(x, y, use_continuity=True, alternative="less")
